#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
# import poker

RANK_NAME		=  {0: "High card",
					1: "1 Pair",
					2: "2 Pairs",
					3: "3 of a kind",
					4: "Straight",
					5: "Flush",
					6: "Full House",
					7: "4 of a kind",
					8: "Straight Flush",
					9: "Royal Straight Flush"}

SUIT_RANK		=  {"Club" 		: 0,
					"Spade"		: 1,
					"Heart"		: 2,
					"Diamond"	: 3}

CARD_BACK		=	chr(0x1F0A0)

CALL			=	0
RAISE			=	1
FOLD			=	2

class Card:
	def __init__(self, raw_data = ["","-1", "", "", "", ""]):
		self.position	=	raw_data[0]
		self.score 		= 	int(raw_data[1])
		self.character 	= 	raw_data[2]
		self.suit 		= 	raw_data[3]
		self.color 		= 	raw_data[4]
		self.icon 		= 	raw_data[5]

class Card_Set:
	def __init__(self, card_set):

		self.card					=	card_set
		self.high_card				=	Card()			#empty card
		self.pair 					= 	0
		self.is_straight			=	False
		self.is_flush				=	False
		self.is_royal_straight		=	False
		self.rank 					= 	0
		self.x_of_a_kind			=	1
		self.sort()

	def deal_from(set_origin: list):
		set_a = []
		for i in range(5):
			new_card_index = random.randrange(len(set_origin))

			while ((set_origin[new_card_index].score < 10)):
				new_card_index = random.randrange(len(set_origin))
			set_a.append(set_origin[new_card_index])

			del set_origin[new_card_index]
		return Card_Set(set_a)

	def deal_1_card_from(self, set_origin):
		new_card_index = random.randrange(len(set_origin))

		while ((set_origin[new_card_index].score < 10)):
				new_card_index = random.randrange(len(set_origin))

		self.card.append(set_origin[new_card_index])
		
		del(set_origin[new_card_index])

	def change_cards(self, change_list, set_origin):
		for i in reversed(change_list):
			del(self.card[i])

			self.deal_1_card_from(set_origin)

	def reset_rank(self):
		self.high_card 				=	Card()
		self.pair					= 	0
		self.is_straight			=	False
		self.is_flush				=	False
		self.is_royal_straight		=	False
		self.rank 					= 	0
		self.x_of_a_kind			=	1

	def rate(self):
		self.reset_rank()

		find_pairs		(self)
		find_sequence	(self)			
		find_same_suit	(self)

		if (self.x_of_a_kind 	==	2):
			self.rank 				=	1		#1 pair
			if (self.pair 		== 	2):
				self.rank 			=	2		#2 pairs

		elif (self.x_of_a_kind ==	3):
			self.rank				=	3		#3 of a kind
			if (self.pair 		== 	4):
				self.rank			=	6		#full house

		elif (self.x_of_a_kind ==	4):
			self.rank				=	7		#4 of a kind

		if (self.is_flush):
			self.rank				=	5		#flush

		if (self.is_straight):
			self.rank				=	4		#straight

			if (self.is_flush):
				self.rank			=	8		#straight flush		

				if (self.high_card.character == "A"):
					self.rank		=	9		#royal straight flush

		if (self.rank == 0):
			self.high_card 		=	self.card[4]

		print("\n" + RANK_NAME[self.rank] + "\nHigh card: " + self.high_card.icon + "\n")

	def print(self):
		self.sort()
		print()
		for card in self.card:
			print(card.icon, end = ' ')
		print()

	def sort(self):
		self.card.sort(key = lambda card: card.score)

class Player:

	def __init__(self, name, card_set):
		self.card_set 	= 	card_set
		# self.status 	=	CALL
		self.name		=	name

	def change_cards_with(self, set_origin):
		print(self.name + ", your set is: ")
		self.card_set.print()
		change_list = standardize(input("\nChange those cards: "))
		print()

		if (change_list != None):
			self.card_set.change_cards(change_list, set_origin)

		self.card_set.print()
		self.card_set.rate()

def is_same_color(card_1, card_2: Card):
	return (card_1.color == card_2.color)

def is_same_score(card_1, card_2: Card):
	return (card_1.score == card_2.score)

def is_same_suit(card_1, card_2: Card):
	return (card_1.suit == card_2.suit)

def is_sequence(card_1, card_2: Card):
	return (card_1.score + 1 == card_2.score)

def new_set():
	new_set = []
	with open("cards.dat", 'r') as f:
		for line in f:
			new_set.append(Card(line.split()))
	return new_set

def find_pairs(set_1 : Card_Set):

	for i in range(len(set_1.card) - 1):
		card_count = 1
		for j in range(i + 1, len(set_1.card)):
			if (is_same_score(set_1.card[i], set_1.card[j])):

				if (set_1.high_card.score < set_1.card[j].score):
					set_1.high_card = set_1.card[j]

				card_count 			+= 	1
				set_1.pair 			+= 	1
				set_1.x_of_a_kind	= 	max(set_1.x_of_a_kind, card_count)

def find_sequence(set_1 : Card_Set):
	if (is_sequence(set_1.card[0], set_1.card[1]) and 
		is_sequence(set_1.card[1], set_1.card[2]) and
		is_sequence(set_1.card[2], set_1.card[3])):

		if (is_sequence(set_1.card[3], set_1.card[4])):
			set_1.is_straight				=	True
			set_1.high_card					=	set_1.card[4]

		if ((set_1.card[4].character 	==	"A") and (set_1.card[0].character == "2")):
			set_1.is_straight				=	True
			set_1.high_card					=	set_1.card[3]
	
def find_same_suit(set_1 : Card_Set):
	if (is_same_suit(set_1.card[0], set_1.card[1]) and 
		is_same_suit(set_1.card[0], set_1.card[2]) and
		is_same_suit(set_1.card[0], set_1.card[3]) and 
		is_same_suit(set_1.card[0], set_1.card[4])):

			set_1.is_flush					=	True
			set_1.high_card					=	set_1.card[4]

def standardize(raw_list : str):
	result = raw_list.split()

	if (result[0] == str(0)):
		return None

	for i in range(len(result)):
		result[i] = int(result[i]) - 1

	return sorted(result)

i = 0
with open('cards.dat', 'r') as fi:
	with open('new_cards.dat', 'w') as fo:
		for line in fi:
			s = str(i) + ' ' + line
			fo.write(s)
			i += 1