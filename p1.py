import requests
import re
from definitions import *

url_get_card_player_1 = 'https://api.thingspeak.com/channels/535405/fields/1.json?api_key=AKL4GEENRWCYKJ65&results=1'

url_update_change_list_player_1 = 'https://api.thingspeak.com/update?api_key=W6H7HC1FYCTUG7PU&field4='

session = requests.Session()
page = session.get(url_get_card_player_1)
raw_data = re.findall(r'"entry_id":\d*,"field1":"(.+?)"', page.text)

card_orders = [int(order) for order in raw_data[0].split()]

set_origin = new_set()

set_temp  = []


for order in card_orders:
	# print(set_origin[order].icon, end = ' ')
	set_temp.append(set_origin[order])
# print()

player_1 = Player("Player 1", Card_Set(set_temp))
player_1.card_set.print()
player_1.card_set.rate()

change_list = input('Change those cards: ')

change_list = change_list.replace(' ', '%20')
url_temp = url_update_change_list_player_1 + change_list
session.get(url_temp)
