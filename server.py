import os
import requests
import re
from definitions import *

#TODO: add the joker into this set
url_update_player_1 = 'https://api.thingspeak.com/update?api_key=W6H7HC1FYCTUG7PU&field1='
url_get_change_list_player_1 = 'https://api.thingspeak.com/channels/535405/fields/4.json?api_key=AKL4GEENRWCYKJ65&results=1'

def is_high_card_better(player_1, player_2: Player):

	card_1 = player_1.card_set.high_card
	card_2 = player_2.card_set.high_card

	if (card_1.score == card_2.score):
		return (SUIT_RANK[card_1.suit] > SUIT_RANK[card_2.suit])
	else:
		return (card_1.score > card_2.score)

def compare_rank(player_list : list):
	print(player_list[0].name + "'s set:")
	player_list[0].card_set.print()
	player_list[0].card_set.rate()

	print(player_list[1].name + "'s set:")
	player_list[1].card_set.print()
	player_list[1].card_set.rate()

	winner = player_list[0]

	for player in player_list:
		if (player.card_set.rank == winner.card_set.rank):
			if (is_high_card_better(player, winner)):
				winner = player

		if (player.card_set.rank > winner.card_set.rank):
			winner = player


	print("Higher: " + winner.name + "\n")

session = requests.Session()

set_origin = new_set()

# print(CARD_BACK, " x", len(set_origin), "\n")

player = []

# sample = [set_origin[9],set_origin[9], set_origin[12], set_origin[12], set_origin[13]]
# player.append(Player("Player 1", Card_Set(sample)))

# sample = [set_origin[10],set_origin[11], set_origin[11], set_origin[11], set_origin[25]]
# player.append(Player("Player 2", Card_Set(sample)))

player.append(Player("Player 1", Card_Set.deal_from(set_origin)))
# player.append(Player("Player 2", Card_Set.deal_from(set_origin)))


player[0].card_set.print()
player[0].card_set.rate()

# s = url_update_player_1

# for card in (player[0].card_set.card):
# 	s += card.position + '%20'

# session.get(s)

page = session.get(url_get_change_list_player_1)
raw_data = re.findall(r'"entry_id":\d*,"field4":"(.+?)"', page.text)

change_list = standardize(raw_data[0])
if (change_list != None):
		player[0].card_set.change_cards(change_list, set_origin)

player[0].card_set.print()
player[0].card_set.rate()

# player[1].card_set.print()
# player[1].card_set.rate()

# compare_rank(player)
		# print("\n\n" + CARD_BACK, " x", len(set_origin), "\n")
# for i in range(3):
# 	print("=========== ROUND", i + 1,"===========")
# 	player[0].change_cards_with(set_origin)
# 	player[1].change_cards_with(set_origin)
# 	compare_rank(player)


# player[1].change_cards_with(set_origin)

# os.system('clear')

# for i in range(3):
# 	change_list = standardize(input("Change those cards: "))

# 	set_a.change_cards(change_list, set_origin)
# 	print("\n\n" + CARD_BACK, " x", len(set_origin), "\n")
# 	set_a.print()

# 	set_a.rate()